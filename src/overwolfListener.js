import Combo from './models/combo'
import {InputTypeEnum, SpellNameEnum, SummonerSpellNameEnum, COMBO_TIMEOUT, DEFAULT_EXECUTION_WINDOW, DEFAULT_IS_OPTIONAL, NULL_COMBO} from './models/enums'
import Input from './models/input'
import Player from './models/player'
import Step from './models/step'
import Talon from '../static/talon.json'
import Annie from '../static/annie.json'
import Leblanc from '../static/leblanc.json'

// this a subset of the features that LoL events provides - however,
// when writing an app that consumes events - it is best if you request
// only those features that you want to handle.
//
// NOTE: in the future we'll have a wildcard option to allow retreiving all
// features
var g_interestedInFeatures = [
  'summoner_info',
  'gameMode',
  // 'teams',
  // 'matchState',
  // 'kill',
  // 'death',
  // 'respawn',
  // 'assist',
  // 'minions',
  // 'level',
  'abilities'
  // 'announcer'
  // 'gold'
];

var playerChampion
var gameMap
var allCombos = {
  "Talon": Talon,
  "Annie": Annie,
  "Leblanc": Leblanc
};
var availableCombos = null

var womboComboAudioId = null
var womboComboAudioURL = "https://s3-us-west-2.amazonaws.com/wombo-combo/wombo_combo4.mp3"
var audioLoadTimer = null

var nullComboTimer = null
var initTimer = null
var resetFinishedComboTimer = null
function init() {
  overwolf.games.events.getInfo(initCallback)
}

function initCallback(info) {
  if (availableCombos == null) {
    playerChampion = info.res.summoner_info.champion;
    // gameMap = info.info.summoner_info.gameMode; TODO: Get from Riot API
    availableCombos = allCombos[playerChampion]
    if (availableCombos != null) {
      console.log("Loaded combos: " + JSON.stringify(availableCombos))

      nullComboTimer = window.setTimeout(sendNullComboInput, COMBO_TIMEOUT)
      window.clearInterval(initTimer)
    }
  }
}

function loadAudio() {
  if (womboComboAudioId == null) {
    console.log("Trying to load audio from: " + womboComboAudioURL)
    overwolf.media.audio.create(womboComboAudioURL, function(result) {
      if (result.status == "success") {
        womboComboAudioId = result.id
        console.log("Loaded audio file: " + womboComboAudioId)
        window.clearInterval(audioLoadTimer)
      } else {
        console.log("Error loading audio file from: " + womboComboAudioURL)
      }
    });
  }
}

function sendNullComboInput() {
  processInput(null)
}

var initialInput = false
var mostRecentTimestamp = null
var currentComboChain = []
var lastPossibleComboList = []

function processInput(input) {
  // console.log("called with:" + JSON.stringify(input) + "; lastPossibleComboList: " + JSON.stringify(lastPossibleComboList))
  if (input == null) {
    if (initialInput) {
      console.log("Sending NULL COMBO")
      EventBus.$emit("input", null)
      EventBus.$emit("combos", {})
      mostRecentTimestamp = null
    }
    currentComboChain = []
    lastPossibleComboList = []
    return;
  }

  if (availableCombos == null) {
    console.log("Combos not loaded yet")
    return;
  }

  // console.log("using real input")

  window.clearTimeout(nullComboTimer)
  nullComboTimer = window.setTimeout(sendNullComboInput, COMBO_TIMEOUT)

  initialInput = true

  var strippedInput = {
    type: input.type,
    name: input.name
  }

  var combosToHighlight = {}
  var combosToCheck = null
  var isFinishedCombo = false
  var stepAddedToComboChain = false
  var savedLastPossibleComboList = []
  var doubleCheckPossibleCombos = false

  if (lastPossibleComboList.length == 0) {
    combosToCheck = availableCombos.combos
  } else {
    doubleCheckPossibleCombos = true
    combosToCheck = lastPossibleComboList
    savedLastPossibleComboList = lastPossibleComboList
    lastPossibleComboList = []
  }

  var currentComboIndex = currentComboChain.length
  // console.log("combosToCheck: " + JSON.stringify(combosToCheck))
  for (var i = 0; i < combosToCheck.length; i++) {
    var combo = combosToCheck[i];
    // console.log("checking: " + combo.name)
    var stepList = combo.steps
    try {
      if (currentComboIndex < stepList.length) {
        var stepToVerify = stepList[currentComboIndex]
        if (stepToVerify.type == input.type && stepToVerify.name == input.name) {
          var executionWindow = 'executionWindow' in stepToVerify ? stepToVerify.executionWindow : DEFAULT_EXECUTION_WINDOW
          // var isOptional = 'isOptional' in stepToVerify ? stepToVerify.isOptional : DEFAULT_IS_OPTIONAL // TODO: ACTUALLY IMPLEMENT
          var elapsed = input.timestamp - mostRecentTimestamp
          // console.log(input.name + " " +mostRecentTimestamp + " " + currentComboIndex + " " + elapsed)
          if ((mostRecentTimestamp == null && currentComboIndex == 0) || checkExecutionWindow(executionWindow, mostRecentTimestamp, input.timestamp)) {
            window.clearTimeout(resetFinishedComboTimer)
            doubleCheckPossibleCombos = false
            lastPossibleComboList.push(combo)
            if (!stepAddedToComboChain) {
              currentComboChain.push(strippedInput)
              stepAddedToComboChain = true
            }
            if (currentComboChain.length == stepList.length) {
              isFinishedCombo = true
              console.log("Finished combo! " + combo.name + " " + JSON.stringify(currentComboChain))
              if (womboComboAudioId != null) {
                console.log("Play audio!")
                overwolf.media.audio.play(womboComboAudioId, function(result){
                  console.log("result: " + result.status)
                });
              }
              mostRecentTimestamp = null
            } else {
              mostRecentTimestamp = input.timestamp
            }
            combosToHighlight[combo.name] = {
              name: combo.name,
              highlight: currentComboChain,
              isFinished: isFinishedCombo
            }
          }
        }
      }
    } catch(err) {
      console.log("Invalid: " + err + "; input: " + JSON.stringify(input));
    }

  }

  if (doubleCheckPossibleCombos && input.type != InputTypeEnum.AUTO_ATTACK) {
    // console.log("double checking...")
    mostRecentTimestamp = null
    currentComboChain = []
    lastPossibleComboList = []
    processInput(input)
  }

  if (!stepAddedToComboChain && input.type == InputTypeEnum.AUTO_ATTACK) {
    // console.log("return early")
    lastPossibleComboList = savedLastPossibleComboList
    return;
  }

  console.log("Sending input: " + JSON.stringify(strippedInput))
  EventBus.$emit("input", strippedInput)
  console.log("Sending combos: " + JSON.stringify(combosToHighlight))
  EventBus.$emit("combos", combosToHighlight)

  if (isFinishedCombo) {
    resetFinishedComboTimer = window.setTimeout(sendNullComboInput, 1500)
    currentComboChain = []
    lastPossibleComboList = []
  }

  // reset if failed
  if (lastPossibleComboList.length == 0) {
    mostRecentTimestamp = null
    currentComboChain = []
  }

  // console.log("exit process input; mostRecentTimestamp: " + JSON.stringify(mostRecentTimestamp) + "; currentComboChain: " + JSON.stringify(currentComboChain) + "; lastPossibleComboList: " + JSON.stringify(lastPossibleComboList))
}

function checkExecutionWindow(range, lastTimestamp, currentTimestamp) {
  var timeElapsed = currentTimestamp - lastTimestamp;
  //console.log(timeElapsed, range.minimum, range.maximum)
  if (timeElapsed >= range.minimum && timeElapsed <= range.maximum) {
    return true;
  }
  return false;
}

var latestInputSpell = null;
var minimumGapSpell = 100; // ms
var latestInputKey = null;
var minimumGapKey = 100; // ms
var latestInputMouseClick = null;
var minimumGapMouse = 400; // ms

var sharedQueue = []
var sharedQueueTimer = []

function putInQueue(input) {
  sharedQueue.push(input)
  sharedQueueTimer.push(window.setTimeout(removeTopOfQueue, 2000))
}

function removeTopOfQueue() {
  sharedQueue.shift()
  sharedQueueTimer.shift()
}

function checkForAbility(spellName) {
  if (sharedQueue.length > 0) {
    var item = sharedQueue[0]
    if (item.name == spellName) {
      sharedQueue.shift()
      var timerToClear = sharedQueueTimer.shift()
      window.clearTimeout(sharedQueueTimer)
      // console.log("calling process input with " + JSON.stringify(item))
      processInput(item)
    }
  }
}

function registerEvents() {
  console.log("REGISTERING EVENTS")
  initTimer = window.setInterval(init, 1000)
  audioLoadTimer = window.setInterval(loadAudio, 3000)

  // general events errors
  overwolf.games.events.onError.addListener(function(info) {
    console.log("Error: " + JSON.stringify(info));
  });

  // "static" data changed (total kills, username, steam-id)
  // This will also be triggered the first time we register
  // for events and will contain all the current information
  overwolf.games.events.onInfoUpdates2.addListener(function(info) {
    // console.log("Info UPDATE: " + JSON.stringify(info));
  });

  // an event triggered
  overwolf.games.events.onNewEvents.addListener(function(info) {
    var timestamp = Date.now();
    // console.log("EVENT FIRED: " + JSON.stringify(info));
    var event = info.events[0];
    if (event.name == "ability") {
      var inputPayload = null;
      var timestamp = Date.now();
      if (event.data == "1") {
        // Q Eventa
        inputPayload = new Input(InputTypeEnum.SPELL, SpellNameEnum.Q, timestamp)
      } else if (event.data == "2") {
        // W Event
        inputPayload = new Input(InputTypeEnum.SPELL, SpellNameEnum.W, timestamp)
      } else if (event.data == "3") {
        // E Event
        inputPayload = new Input(InputTypeEnum.SPELL, SpellNameEnum.E, timestamp)
      } else if (event.data == "4") {
        // R Event
        inputPayload = new Input(InputTypeEnum.SPELL, SpellNameEnum.R, timestamp)
      }

      if (inputPayload != null) {
        if (latestInputSpell != null) {
          if (inputPayload.type == latestInputSpell.type && inputPayload.name == latestInputSpell.name) {
            if (inputPayload.timestamp - latestInputSpell.timestamp < minimumGapSpell) {
              return
            }
          }
        }

        latestInputSpell = inputPayload
        putInQueue(inputPayload)
      }

    } else if (event.name == "usedAbility") {
      var eventData = null;
      try {
        eventData = JSON.parse(event.data);
      } catch(err) {
        console.log("eventData JSON parsing error");
      }
      var inputPayload = null;
      if (eventData.type == "1") {
        // Q Event
        checkForAbility(SpellNameEnum.Q)
      } else if (eventData.type == "2") {
        // W Event
        checkForAbility(SpellNameEnum.W)
      } else if (eventData.type == "3") {
        // E Event
        checkForAbility(SpellNameEnum.E)
      } else if (eventData.type == "4") {
        // R Event
        checkForAbility(SpellNameEnum.R)
      }
    }
  });

  overwolf.games.inputTracking.onKeyDown.addListener(function(info) {
    var timestamp = Date.now();
    // console.log("KEY DOWN: " + JSON.stringify(info));
    if (info.onGame) {
      var inputPayload = null;
      if (info.key == "68") {
        // D Event
        inputPayload = new Input(InputTypeEnum.SUMMONER_SPELL, SummonerSpellNameEnum.D, timestamp)
      } else if (info.key == "70") {
        // F Event
        inputPayload = new Input(InputTypeEnum.SUMMONER_SPELL, SummonerSpellNameEnum.F, timestamp)
      } else if (info.key == "49") {
        // 1 Event
        inputPayload = new Input(InputTypeEnum.ITEM, "ITEM", timestamp)
      } else if (info.key == "50") {
        // 2 Event
        inputPayload = new Input(InputTypeEnum.ITEM, "ITEM", timestamp)
      } else if (info.key == "51") {
        // 3 Event
        inputPayload = new Input(InputTypeEnum.ITEM, "ITEM", timestamp)
      } else if (info.key == "52") {
        // 4 Event
        inputPayload = new Input(InputTypeEnum.ITEM, "ITEM", timestamp)
      } else if (info.key == "53") {
        // 5 Event
        inputPayload = new Input(InputTypeEnum.ITEM, "ITEM", timestamp)
      } else if (info.key == "54") {
        // 6 Event
        inputPayload = new Input(InputTypeEnum.ITEM, "ITEM", timestamp)
      }
      if (inputPayload != null) {
        if (latestInputKey != null) {
          if (inputPayload.type == latestInputKey.type && inputPayload.name == latestInputKey.name) {
            if (inputPayload.timestamp - latestInputKey.timestamp < minimumGapKey) {
              return
            }
          }
        }
        latestInputKey = inputPayload

        // console.log("calling process input with " + JSON.stringify(inputPayload))
        processInput(inputPayload)
      }
    }
  });

  overwolf.games.inputTracking.onMouseDown.addListener(function(info) {
    var timestamp = Date.now();
    // console.log("MOUSE DOWN: " + JSON.stringify(info));
    if (info.onGame) {
      var inputPayload = null;
      var timestamp = Date.now();
      if (info.button == "right") {
        // Right click Event
        inputPayload = new Input(InputTypeEnum.AUTO_ATTACK, "AUTO_ATTACK", timestamp)
      }

      if (inputPayload != null) {
        if (latestInputMouseClick != null) {
          if (inputPayload.type == latestInputMouseClick.type && inputPayload.name == latestInputMouseClick.name) {
            if (inputPayload.timestamp - latestInputMouseClick.timestamp < minimumGapMouse) {
              return
            }
          }
        }
        latestInputMouseClick = inputPayload

        // console.log("calling process input with " + JSON.stringify(inputPayload))
        processInput(inputPayload)
      }

    }
  });
}

function gameLaunched(gameInfoResult) {
  if (!gameInfoResult) {
    return false;
  }

  if (!gameInfoResult.gameInfo) {
    return false;
  }

  if (!gameInfoResult.runningChanged && !gameInfoResult.gameChanged) {
    return false;
  }

  if (!gameInfoResult.gameInfo.isRunning) {
    return false;
  }

  // NOTE: we divide by 10 to get the game class id without it's sequence number
  if (Math.floor(gameInfoResult.gameInfo.id/10) != 5426) {
    return false;
  }

  console.log("LoL Launched");
  return true;

}

function gameRunning(gameInfo) {

  if (!gameInfo) {
    return false;
  }

  if (!gameInfo.isRunning) {
    return false;
  }

  // NOTE: we divide by 10 to get the game class id without it's sequence number
  if (Math.floor(gameInfo.id/10) != 5426) {
    return false;
  }

  console.log("LoL running");
  return true;

}


function setFeatures() {
  overwolf.games.events.setRequiredFeatures(g_interestedInFeatures, function(info) {
    if (info.status == "error")
    {
      //console.log("Could not set required features: " + info.reason);
      //console.log("Trying in 2 seconds");
      window.setTimeout(setFeatures, 2000);
      return;
    }

    console.log("Set required features:");
    console.log(JSON.stringify(info));
  });
}


// Start here
overwolf.games.onGameInfoUpdated.addListener(function (res) {
  if (gameLaunched(res)) {
    registerEvents();
    setTimeout(setFeatures, 1000);
  }
  console.log("onGameInfoUpdated: " + JSON.stringify(res));
});

overwolf.games.getRunningGameInfo(function (res) {
  if (gameRunning(res)) {
    registerEvents();
    setTimeout(setFeatures, 1000);
  }
  console.log("getRunningGameInfo: " + JSON.stringify(res));
});
