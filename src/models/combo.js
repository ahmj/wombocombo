export default class Combo {
    constructor(name, steps, description, tags, map) {
        this.name = name;
        this.steps = steps; // List of pre-defined steps
        this.description = description; // Tooltip
        this.tags = tags; // Laning, teamfighting, assassinating, safe, fastest burst, highest damage
        this.map = map; // ARAM only map (uses snowball)
    }
}
