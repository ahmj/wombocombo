export default class Step {
    constructor(type, name, executionWindow, isOptional) {
        this.type = type; // Spell, Auto-Attack, Item, Summoner Spell
        this.name = name; // Q, W, E, R, "Hextech Gunblade"
        this.executionWindow = executionWindow; // milliseconds. e.g. [0, 500], [1500, 4000] for Leblanc passive
        this.isOptional = isOptional; // Some spells aren't used in combos every time
    }
}
