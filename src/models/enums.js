export const InputTypeEnum = {
    SPELL : "SPELL",
    SUMMONER_SPELL : "SUMMONER_SPELL",
    ITEM : "ITEM",
    AUTO_ATTACK : "AUTO_ATTACK",
}

export const SpellNameEnum = {
    Q : "Q",
    W : "W",
    E : "E",
    R : "R"
}

export const SummonerSpellNameEnum = {
    D : "D",
    F : "F"
}

export const COMBO_TIMEOUT = 2000 // ms (3 seconds)
export const DEFAULT_EXECUTION_WINDOW = { // default window between performing 2 spells in a combo is 1 second, with no minimum time
    minimum: 0,
    maximum: 1500
}
export const DEFAULT_IS_OPTIONAL = false

