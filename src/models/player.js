export default class Player {
    constructor(pinnedCombos, filteredTags, playedChampion, summonerId, currentSummonerSpells) {
        this.pinnedCombos = pinnedCombos;
        this.filteredTags = filteredTags;
        this.playedChampion = playedChampion;
        this.summonerId = summonerId;
        this.currentSummonerSpells = currentSummonerSpells;
    }
}
