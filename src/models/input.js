export default class Input {
    constructor(type, name, timestamp) {
        // Type and name are same as in step.js
        this.type = type; // Spell, Auto-Attack, Item, Summoner Spell
        this.name = name; // Q, W, E, R, "Hextech Gunblade"
        this.timestamp = timestamp;
    }
}
