// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'

require('./assets/main.scss');

Vue.config.productionTip = false

window.EventBus = new Vue()

// load the combo definitions
window.Combos = {}
window.Combos.Talon = require('../static/talon').talon
window.Combos.Annie = require('../static/annie').annie
window.Combos.Leblanc = require('../static/leblanc').leblanc
//window.Combos.Riven = require('../static/riven').riven


const talonSpells = require('../static/champions').champions.data.Talon.spells
const annieSpells = require('../static/champions').champions.data.Annie.spells
const leblancSpells = require('../static/champions').champions.data.Leblanc.spells
const rivenSpells = require('../static/champions').champions.data.Riven.spells

const baseURL = 'http://ddragon.leagueoflegends.com/cdn/7.21.1/img/spell/'
const noItem = 'https://github.com/taycaldwell/whos-that-champion/blob/master/FlaskApp/FlaskApp/static/images/item/NoItem.png?raw=true'
const itemLink = (id) => { id ? 'http://ddragon.leagueoflegends.com/cdn/7.21.1/img/item/'+id+'.png' : noItem }

const autoAttackIcon = "https://imgur.com/DGYsDJ0.png";
const igniteIcon = "SummonerDot.png";
const flashIcon = "SummonerFlash.png";
window.Spells = { 
	"Talon": {
		"Q": baseURL + talonSpells[0].image.full,
		"W": baseURL + talonSpells[1].image.full,
		"E": baseURL + talonSpells[2].image.full,
		"R": baseURL + talonSpells[3].image.full,
		"AUTO_ATTACK": autoAttackIcon,
		"ITEM": itemLink(3077),
		"D": baseURL + igniteIcon,
		"F":baseURL + flashIcon
	},
	"Annie": {
		"Q": baseURL + annieSpells[0].image.full,
		"W": baseURL + annieSpells[1].image.full,
		"E": baseURL + annieSpells[2].image.full,
		"R": baseURL + annieSpells[3].image.full,
		"AUTO_ATTACK": autoAttackIcon,
		"ITEM": itemLink(null),
		"D": baseURL + igniteIcon,
		"F":baseURL + flashIcon
	},
	"Leblanc": {
		"Q": baseURL + leblancSpells[0].image.full,
		"W": baseURL + leblancSpells[1].image.full,
		"E": baseURL + leblancSpells[2].image.full,
		"R": baseURL + leblancSpells[3].image.full,
		"AUTO_ATTACK": autoAttackIcon,
		"ITEM": itemLink(3146),
		"D": baseURL + igniteIcon,
		"F":baseURL + flashIcon
	},
	"Riven": {
		"Q": baseURL + rivenSpells[0].image.full,
		"W": baseURL + rivenSpells[1].image.full,
		"E": baseURL + rivenSpells[2].image.full,
		"R": baseURL + rivenSpells[3].image.full,
		"AUTO_ATTACK": autoAttackIcon,
		"ITEM": itemLink(3077),
		"D": baseURL + igniteIcon,
		"F":baseURL + flashIcon
	}
	
}
console.log(Combos)	
console.log(Spells)

try {
  require('./overwolfListener')
  overwolf.windows.getCurrentWindow((a) => { overwolf.windows.maximize(a.window.id) })
} catch (e) {
  setTimeout(() => {
    setTimeout(() => {
      EventBus.$emit('input', {
        type: 'SPELL', name: 'Q', timestamp: 1,
      })
      EventBus.$emit('combos', {
        'Leap': {
          isFinished: false, highlight: [{type: 'SPELL', name: 'Q', timestamp: 1}],
        }
      })
    }, 0)

    setTimeout(() => {
      EventBus.$emit('input', {
        type: 'SUMMONER_SPELL', name: 'F', timestamp: 2,
      })
      EventBus.$emit('combos', {
        'Leap': {
          isFinished: false, highlight: [{type: 'SPELL', name: 'Q', timestamp: 1}, {type: 'SUMMONER_SPELL', name: 'F', timestamp: 2}],
        }
      })
    }, 2000)
  }, 1000)
}


/* eslint-disable no-new */
new Vue({
  el: '#app',
  template: '<App/>',
  components: { App }
})
