export const leblanc = {
  "combos": {
    "Standard": {
      "name": "Standard",
      "steps": [
        {
          "name": "E",
          "type": "SPELL"
        },
        {
          "name": "Q",
          "type": "SPELL",
          "executionWindow": {"minimum": 1300, "maximum": 5300}
        },
        {
          "name": "W",
          "type": "SPELL"
        },
        {
          "name": "D",
          "type": "SUMMONER_SPELL"
        }
      ],
      "tags": [
        "General",
        "Laning",
        "Teamfighting"
      ],
      "description": "Standard harrass in lane"
    },
    "Farming / Poke": {
      "name": "Farming / Poke",
      "steps": [
        {
          "name": "W",
          "type": "SPELL"
        },
        {
          "name": "Q",
          "type": "SPELL",
          "executionWindow": {"minimum": 1300, "maximum": 5300}
        }
      ],
      "tags": [
        "General",
        "Laning",
        "Teamfighting"
      ],
      "description": "Basic LeBlanc farming / poking combo"
    },
    "Double W": {
      "name": "Double W",
      "steps": [
        {
          "name": "W",
          "type": "SPELL"
        },
        {
          "name": "R",
          "type": "SPELL"
        },
        {
          "name": "W",
          "type": "SPELL"
        },
        {
          "name": "Q",
          "type": "SPELL"
        }
      ],
      "tags": [
        "General",
        "Laning",
        "Teamfighting"
      ],
      "description": "Basic Leblanc Combo Wombo v2"
    },
    "2Chainz": {
      "name": "2Chainz",
      "steps": [
        {
          "name": "E",
          "type": "SPELL"
        },
        {
          "name": "R",
          "type": "SPELL"
        },
        {
          "name": "E",
          "type": "SPELL"
        },
        {
          "name": "Q",
          "type": "SPELL"
        }
      ],
      "tags": [
        "General",
        "Laning",
        "Teamfighting"
      ],
      "description": "Basic Leblanc Combo Wombo v2"
    }
  }
}
