export const talon = {
  "combos": {
    "Standard": {
      "name": "Standard",
      "steps": [
        {
          "name": "W",
          "type": "SPELL"
        },
        {
          "name": "Q",
          "type": "SPELL"
        },
        {
          "name": "AUTO_ATTACK",
          "type": "AUTO_ATTACK"
        }
      ],
      "tags": [
        "General",
        "Laning",
        "Teamfighting"
      ],
      "description": "Allows passive proc without ultimate. Bread and butter. Use while laning / skirmishing."
    },
    "Leap": {
      "name": "Leap",
      "steps": [
        {
          "name": "Q",
          "type": "SPELL"
        },
        {
          "name": "W",
          "type": "SPELL"
        },
        {
          "name": "AUTO_ATTACK",
          "type": "AUTO_ATTACK"
        },
        {
          "name": "AUTO_ATTACK",
          "type": "AUTO_ATTACK"
        }
      ],
      "tags": [
        "General",
        "Assasinating",
        "High Damage"
      ],
      "description": "Use when enemy not in range for rake. Allows you to get close for double rake."
    },
    "All In": {
      "name": "All In",
      "steps": [
        {
          "name": "W",
          "type": "SPELL"
        },
        {
          "name": "Q",
          "type": "SPELL"
        },
        {
          "name": "D",
          "type": "SUMMONER_SPELL"
        },
        {
          "name": "AUTO_ATTACK",
          "type": "AUTO_ATTACK"
        }
      ],
      "tags": [
        "General",
        "Assasinating",
        "High Burst"
      ],
      "description": "All in with ignite."
    },
    "Flower": {
      "name": "Flower",
      "steps": [
        {
          "name": "W",
          "type": "SPELL"
        },
        {
          "name": "Q",
          "type": "SPELL"
        },
        {
          "name": "R",
          "type": "SPELL"
        },
        {
          "name": "AUTO_ATTACK",
          "type": "AUTO_ATTACK"
        }
      ],
      "tags": [
        "General",
        "Assasinating",
        "High Burst"
      ],
      "description": "Also called the Fast Combo, the Flower Combo is Talon's fastest combo"
    },
    "Full Burst": {
      "name": "Full Burst",
      "steps": [
        {
          "name": "W",
          "type": "SPELL"
        },
        {
          "name": "R",
          "type": "SPELL"
        },
        {
          "name": "AUTO_ATTACK",
          "type": "AUTO_ATTACK"
        },
        {
          "name": "Q",
          "type": "SPELL"
        },
        {
          "name": "AUTO_ATTACK",
          "type": "AUTO_ATTACK"
        }
      ],
      "tags": [
        "Assasinating",
        "High Damage"
      ],
      "description": "Talon's highest damage combo"
    }
  }
}
