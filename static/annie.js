export const annie = {
  "combos": {
    "Standard": {
      "name": "Standard",
      "steps": [
        {
          "name": "R",
          "type": "SPELL"
        },
        {
          "name": "Q",
          "type": "SPELL"
        },
        {
          "name": "W",
          "type": "SPELL"
        },
        {
          "name": "D",
          "type": "SUMMONER_SPELL"
        }
      ],
      "tags": [
        "General",
        "Laning",
        "Teamfighting"
      ],
      "description": "Basic Annie Combo Wombo"
    },
    "Standard 2": {
      "name": "Standard 2",
      "steps": [
        {
          "name": "R",
          "type": "SPELL"
        },
        {
          "name": "W",
          "type": "SPELL"
        },
        {
          "name": "Q",
          "type": "SPELL"
        },
        {
          "name": "D",
          "type": "SUMMONER_SPELL"
        }
      ],
      "tags": [
        "General",
        "Laning",
        "Teamfighting"
      ],
      "description": "Basic Annie Combo Wombo v2"
    },
    "Standard 3": {
      "name": "Standard 3",
      "steps": [
        {
          "name": "Q",
          "type": "SPELL"
        },
        {
          "name": "R",
          "type": "SPELL"
        },
        {
          "name": "W",
          "type": "SPELL"
        },
        {
          "name": "D",
          "type": "SUMMONER_SPELL"
        }
      ],
      "tags": [
        "General",
        "Laning",
        "Teamfighting"
      ],
      "description": "Basic Annie Combo Wombo v3"
    }
  }
}
